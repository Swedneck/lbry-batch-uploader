#!/bin/bash
# TODO: implement the below warning
# WARNING: This script will copy all uploaded files to a new location, so that
# the original file is not deleted when you delete it from LBRY, and so that
# deleting the original file does not make it unacessible to LBRY.

thumbnail_url() {
	ipfs_gateway="https://ipfs.io/ipfs"

	ipfs_cid="$(ipfs add -Q "$thumbnail_path")"
	url="$ipfs_gateway/$ipfs_cid"
	echo $url
}

extension="png"
files=$(ls *.$extension | tr ' ' '_')
bid_amount="0.0001"
tags=("photography" "wallpaper")
license="Creative Commons Attribution-ShareAlike 4.0 International"
license_url="https://creativecommons.org/licenses/by-sa/4.0/"
channel_id="61438b62d514fe7a21b489a6f8c1b76738d83720"


for file in $files; do
	file_path="$(readlink -f "$file")"
	file_name="$(basename $file | cut -d'.' -f1)"
	thumbnail_path="$(echo $file_path | tr $extension 'jpg')"
	thumbnail_url=$(thumbnail_url)
	echo "filename :" $file_name
	echo "file path:" $file_path
	echo "thumb url:" $thumbnail_url
	lbrynet publish "$file_name" --title="$file_name" --file_path="$file_path" \
		--bid=$bid_amount --tags=$tags --thumbnail_url=$thumbnail_url \
		--license=$license --license_url=$license_url --channel_id=$channel_id
done
